# Prueba técnica de Maquinando


Debes crear un juego tipo concentrese. 
Las condiciones son: 
* Puedes utilizar jQuery y otras librerías de terceros, que ayuden a mejorar la experiencia del juego
* Toda acción en el juego debe generar una reacción
* Puedes crear parejas con imágenes de lo que más te guste. 


La entrega de la prueba debe ser así. 
* Hacer fork de este repositorio,
* Sube tu código 
* Hacer un pull request
